<?php

namespace Tests\Feature;

use App\Models\Property;
use Tests\TestCase;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PropertyTest extends TestCase
{
    use WithFaker;

    public function test_property_index_api()
    {
        $this->json('get', 'api/property')
         ->assertStatus(200)
         ->assertJsonStructure(
             [
            '*' => ['id','name','real_state_type','city','country']
            ]
         );
    }

    public function test_property_store_api()
    {
        $payload = $this->setPayload();
        
        $this->json('post', 'api/property', $payload)
         ->assertStatus(201)
         ->assertJsonStructure(
             $this->validJsonResponse()
         );
        $this->assertDatabaseHas('properties', $payload);
    }    

    public function test_property_show_api()
    {
        $payload = Property::create($this->setPayload());
                   
        $this->json('get', "api/property/".$payload->id)
            ->assertStatus(200)
            ->assertJsonStructure(
                $this->validJsonResponse()
            );
    }

    public function test_property_update_api()
    {
        $property = Property::create($this->setPayload());
        $payload = $this->setPayload();
                   
        $this->json('put', "api/property/".$property->id,$payload)
            ->assertStatus(200)
            ->assertJsonStructure(
                $this->validJsonResponse()
            );
    }

    public function test_property_destroy_api()
    {
        $payload = Property::create($this->setPayload());
                   
        $this->json('delete', "api/property/".$payload->id)
            ->assertStatus(200)
            ->assertJsonStructure(
                $this->validJsonResponse()
            );
    }

    public function setPayload()
    {
        return [
            "name" => $this->faker->name(),
            "street" => $this->faker->streetAddress(),
            "external_number" => $this->faker->numerify('############'),
            "internal_number" => $this->faker->numerify('############'),
            "neighborhood" => $this->faker->address(),
            "city" => $this->faker->city(),
            "country" => $this->faker->countryCode(),
            "real_state_type" => $this->faker->randomElement([
                "house",
                "department",
                "land",
                "commercial_ground"
            ]),
            "rooms" => $this->faker->randomNumber(1),
            "bathrooms" => $this->faker->randomNumber(1, true),
            "comments" => $this->faker->sentence(5)
        ];
    }

    public function validJsonResponse()
    {
        return Schema::getColumnListing((new Property())->getTable());
    }
}
