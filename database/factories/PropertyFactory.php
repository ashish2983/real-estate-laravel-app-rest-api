<?php

namespace Database\Factories;

use App\Models\Property;
use Illuminate\Database\Eloquent\Factories\Factory;

class PropertyFactory extends Factory
{
    protected $model = Property::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->name(),
            "street" => $this->faker->streetAddress(),
            "external_number" => $this->faker->numerify('############'),
            "internal_number" => $this->faker->numerify('############'),
            "neighborhood" => $this->faker->address(),
            "city" => $this->faker->city(),
            "country" => $this->faker->countryCode(),
            "real_state_type" => $this->faker->randomElement([
                "house",
                "department",
                "land",
                "commercial_ground"
            ]),
            "rooms" => $this->faker->randomNumber(1),
            "bathrooms" => $this->faker->randomNumber(1,true),
            "comments" => $this->faker->sentence(5)
        ];
    }
}
