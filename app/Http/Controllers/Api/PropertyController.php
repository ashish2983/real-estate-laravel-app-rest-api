<?php

namespace App\Http\Controllers\Api;

use App\Models\Property;
use App\Http\Controllers\Controller;
use App\Http\Requests\PropertyRequest;

class PropertyController extends Controller
{
    public function index()
    {
        return Property::select('id','name','real_state_type','city','country')->latest()->get();
    }

    public function store(PropertyRequest $request)
    {
        return Property::create($request->validated());
    }

    public function show($id)
    {
        return Property::find($id) ?? [];
    }

    public function update(PropertyRequest $request, $id)
    {
        $property = Property::find($id);
        if(is_null($property)){
            return response()->json([]);
        }
        $property->update($request->validated());
        return $property;
    }

    public function destroy($id)
    {
        $property = Property::find($id);
        if(is_null($property)){
            return response()->json([]);
        }
        $property->delete();
        return $property;
    }
}
