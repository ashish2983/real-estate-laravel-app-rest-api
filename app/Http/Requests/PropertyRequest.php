<?php

namespace App\Http\Requests;

use LVR\CountryCode\Two;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:128',
            'real_state_type' => ['required',Rule::in(['house', 'department','land','commercial_ground'])],
            'street' => 'required|string|max:128',
            'external_number' => 'required|alpha_dash|max:12',
            'internal_number' => 'required|alpha_dash|max:12',
            'neighborhood' => 'required|string|max:128',
            'city' => 'required|string|max:64',
            'country' => ['required',new Two],
            'rooms' => 'required|numeric',
            'bathrooms' => 'required|numeric',
            'comments' => ''
        ];
    }
}
